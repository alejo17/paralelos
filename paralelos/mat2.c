#include "mpi.h" 
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#define DIM 5

int main(int argc, char *argv[])
{

      int i, j, n=10; 
      int nlocal;        /* Number of locally stored rows of A */ 
      double *fb;
      double a[DIM][DIM], b[DIM], x[DIM];     /* Will point to a buffer that stores the entire vector b */ 
      int npes, myrank; 
      MPI_Status status; 

       MPI_Init(&argc,&argv);

     /* Get information about the communicator */ 
     MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
     MPI_Comm_size(MPI_COMM_WORLD, &npes);  

     /* Allocate the memory that will store the entire vector b */ 
     fb = (double*)malloc(n*sizeof(double)); 

     nlocal = n/npes; 

     /* Gather the entire vector b on each processor using MPI's ALLGATHER operation */ 
     MPI_Allgather(b, nlocal, MPI_DOUBLE, fb, nlocal, MPI_DOUBLE, MPI_COMM_WORLD); 

     /* Perform the matrix-vector multiplication involving the locally stored submatrix */ 
     for (i=0; i<nlocal; i++) { 
       x[i] = 0.0; 
       for (j=0; j<n; j++) 
         x[i] += a[i][j]*fb[j]; 
     } 

      for (i=0; i<nlocal; i++) { 
       //for (j=0; j<n; j++) 
          printf ("%2.0f ", x[i]);
        printf ("\n");
      }

     free(fb);

     MPI_Finalize();   
}//end main 