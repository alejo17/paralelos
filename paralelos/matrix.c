#include <stdio.h>
 #include <stdlib.h>
 #include <time.h>
 
 int min (int x, int y)
 {
     int temp;
     if (x < y)
         temp = x;
     else
         temp = y;
     return temp;
 }
 
 int main()
 {
     int i, i1, j, j1, k, k1, n=1000, block=50;
     //scanf("%d", &n);
     //scanf("%d %d", &n, &block);
 
     int **A = malloc(sizeof *A * n);
     int **B = malloc(sizeof *B * n);
     int **C = malloc(sizeof *C * n);
     srand(time(NULL));
 
     for(i = 0; i < n; i++)
 		A[i] = malloc(sizeof * A[i]*n);
 	for(i = 0; i < n; i++)
         B[i] = malloc(sizeof * B[i]*n);
 	for(i = 0; i < n; i++)
 		C[i] = malloc(sizeof * C[i]*n);
 
     for(i = 0; i < n; i++)
         for(j = 0; j < n; j++)
             A[i][j] = rand() % 100 + 1;
     for(i = 0; i < n; i++)
         for(j = 0; j < n; j++)
             B[i][j] = rand() % 100 + 1;
 	for(i = 0; i < n; i++)
         for(j = 0; j < n; j++)
             C[i][j] = 0;
 
     clock_t start = clock();
     
 /*    for(i = 0; i < n; i++)
 	{
 		for(j = 0; j < n; j++)
 		{
 			for(k = 0; k < n; k++)
 			{
 				C[i][j] += A[i][k] * B[k][j];
 			}
 		}
 	}*/
     for(i1 = 0; i1 < (n/block); i1++)
 	{
 		for(j1 = 0; j1 < (n/block); j1++)
 		{
 			for(k1 = 0; k1 < (n/block); k1++)
 			{
 				for(i = i1; i < min(i1+block,n); i++)
 				{
 					for(j = j1; j < min(j1+block,n); j++)
 					{
 						for(k = k1; k < min(k1+block,n); k++)
 						{
 							C[i][j] += A[i][k] * B[k][j];
 						}
 					}
 				}
 			}
 		}
     }
 
     clock_t end = clock();
     float time;
     time = (float)(end - start) /CLOCKS_PER_SEC;
     printf("%f\n", time);
     return 0;
 } 