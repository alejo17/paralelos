#include <omp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define info "lf"
#define max FLT_MAX
#define min FLT_MIN

#define TOSS_FMT "lld"
#define atotoss_t(s) atoll(s)
typedef unsigned long long int toss_t;

#define STATE_SIZE 8

static toss_t QTY_POINTS = 0;

void usage(const char *prog)
{
	fprintf(stderr, "Uso: %s -h | -n QTY POINTS\n", prog);
	exit(1);
}

void get_options(int argc, char **argv)
{
	int op;
	while ((op = getopt(argc, argv, "hn:")) != -1) {
		switch (op) {
			case 'n':
				QTY_POINTS = atotoss_t(optarg);
				break;
			case 'h':
			default:
				usage(argv[0]);
		}
	}

	if (QTY_POINTS <= 0)
		usage(argv[0]);
}

int main(int argc, char *argv[])
{
	get_options(argc, argv);


	toss_t sum = 0;
	#pragma omp parallel
	{
		char statebuf[STATE_SIZE];
		struct random_data rd;
		initstate_r(omp_get_thread_num(),
				statebuf, STATE_SIZE, &rd);

		#pragma omp for reduction(+:sum)
		for (int i = 0; i < QTY_POINTS; i++) {
			int32_t xx, yy;
			random_r(&rd, &xx);
			random_r(&rd, &yy);
			double x = 2.0 * (double)xx / RAND_MAX - 1.0;
			double y = 2.0 * (double)yy / RAND_MAX - 1.0;
			if (x*x + y*y < 1)
				sum += 1;
		}
	}

	printf("%"info"\n", (float)sum/QTY_POINTS * 4.0);

	return 0;
}