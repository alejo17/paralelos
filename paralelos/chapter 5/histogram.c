#include <float.h>
#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define info "f"
#define max FLT_MAX
#define min FLT_MIN

static int INFO_LEN = 10;
static int BIN_LEN = 4;

float *get_info(int len, float *limits)
{
	float *data = malloc(sizeof(float) * len);
	float *p = data;
	while (len--) {
		scanf("%" info, p);
		if (*p < limits[0])
			limits[0] = *p;
		else if (*p > limits[1])
			limits[1] = *p;
		p++;
	}
	return data;
}

int *get_bin(int len)
{
	int *cache = malloc(sizeof(int) * len);
	memset(cache, 0, sizeof(int) * len);
	return cache;
}

void histogram(float *data, int INFO_LEN, float *limits,int *bucket, int BIN_LEN)
{
	double dmin = (double)limits[0];
	double delt = (double)(limits[1] - dmin);

	#pragma omp parallel
	{
		int tid = omp_get_thread_num();
		int numt = omp_get_num_threads();
		int *local_bin[numt];

		local_bin[tid] = get_bin(BIN_LEN);

		#pragma omp for nowait
		for (int i = 0; i < INFO_LEN; i++) {
			double r = BIN_LEN * (data[i] - dmin) / delt;
			int b = (int)(r);
			if (b == BIN_LEN)
				b--;
			if (b >= 0 && b < BIN_LEN)
				local_bin[tid][b]++;
		}

		for (int i = 0; i < BIN_LEN; i++) {
			#pragma omp atomic
			bucket[i] += local_bin[tid][i];
		}
	}
}

int main(int argc, char *argv[])
{
	float limits[2] = {max, min};
	float *datain = get_info(INFO_LEN, limits);
	int *bucket = get_bin(BIN_LEN);

	histogram(datain, INFO_LEN, limits, bucket, BIN_LEN);

	printf("Histogram:");
	for (int i = 0; i < BIN_LEN; i++)
		printf(" %d", bucket[i]);
	

	printf("\n");

	return 0;
}