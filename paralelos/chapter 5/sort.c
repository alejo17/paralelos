#include <omp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static int QTY_POINTS = 0;
static bool IF_SORT = false;

void count_sort(int a[], int n)
{
	int i, j, count;
	int *temp = malloc(n*sizeof(int));

	#pragma omp parallel for shared(a, n, temp) private(i, j, count)
	for (i = 0; i < n; i++) {
		count = 0;
		for (j = 0; j < n; j++)
			if (a[j] < a[i])
				count++;
			else if (a[j] == a[i] && j < i)
				count++;
		temp[count] = a[i];
	}

	#pragma omp parallel for shared(a, n, temp) private(i)
	for (i = 0; i < n; i++)
		a[i] = temp[i];

	free(temp);
}

void print_v(int a[], int n)
{
	for (int i = 0; i < n; i++)
		printf("%d\n", a[i]);
}

void usage(char *prog)
{
	fprintf(stderr, "Uso: %s -h | -n QTY_POINTS [-q]\n", prog);
	fprintf(stderr, "    -n      Quantity of points.\n");
	fprintf(stderr, "    -q      Usar sort.\n");
	exit(1);
}

void get_options(int argc, char *argv[])
{
	int opt;
	while ((opt = getopt(argc, argv, "hn:q")) != -1) {
		switch (opt) {
			case 'h':
				usage(argv[0]);
				break;
			case 'n':
				QTY_POINTS = atoi(optarg);
				break;
			case 'q':
				IF_SORT = true;
				break;
			default:
				usage(argv[0]);
				break;
		}
	}

	if (QTY_POINTS <= 0)
		usage(argv[0]);
}

int cmp(const void *aa, const void *bb)
{
	int a = *(int*)aa;
	int b = *(int*)bb;
	return a - b;
}

int main(int argc, char *argv[])
{
	int n, *a;

	get_options(argc, argv);

	n = QTY_POINTS;
	a = malloc(n*sizeof(int));
	for (int i = 0; i < n; i++)
		scanf("%d", &a[i]);

	if (IF_SORT)
		qsort(a, n, sizeof(int), cmp);
	else
		count_sort(a, n);

	print_v(a, n);

	return 0;
}